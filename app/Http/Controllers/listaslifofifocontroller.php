<?php

namespace App\Http\Controllers;

use SplDoublyLinkedList;

use Illuminate\Http\Request;

class listaslifofifocontroller extends Controller
{
    public function ListasLifoFifo(){
          $listas = new SplDoublyLinkedList;  
          $listas->setIteratorMode(SplDoublyLinkedList::IT_MODE_LIFO);
          
          $listas->push(1);
          $listas->push(2);
          $listas->push(3);
          $listas->push(4);
          $listas->push(5);

         // print_r($lista);
        echo "<h2>listas lifo(pila)</h2>";
        $listas->rewind();
          while ($listas->valid()) {
             echo $listas->current()."<br>";
              $listas->next();
          }

          $listas->setIteratorMode(SplDoublyLinkedList::IT_MODE_FIFO);
          echo "<h2>listas fifo(cola)</h2>";
          $listas->rewind();
          while ($listas->valid()) {
             echo $listas->current()."<br>";
              $listas->next();
          }
    }

}