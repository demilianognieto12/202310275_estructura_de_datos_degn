<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\apuntadoresController;
use App\Http\Controllers\MemoriaController;
use App\Http\Controllers\RecursividadController;
use App\Http\Controllers\listaslifofifocontroller;
use App\Http\Controllers\AgregarListaIndiceController;
use App\Http\Controllers\ListaInicioFinController;
use App\Http\Controllers\interfazgraficaController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/apuntadores', [apuntadoresController:: class, 'apuntadores']);

Route::get('/memoria', [MemoriaController:: class, 'memoria']);

Route::get('/recursividad', [RecursividadController:: class, 'incrementable']);

Route::get('/interfaz', [interfazgraficaController:: class, 'Inicio']);

Route::get('/agregar-lista-indice', [AgregarListaIndiceController:: class, 'AgregarLista']);

Route::get('/lista-inicio-fin', [ListaInicioFinController:: class, 'ListaInicioFin']);

Route::get('/listas-lifo-fifo', [listaslifofifocontroller:: class, 'listaslifofifo']);
?>